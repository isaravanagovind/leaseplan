package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import starter.bindings.InvalidResponse;
import starter.bindings.ProductSearch;
import starter.utils.Constants;

public class invalidProductStepDefinitions {

    @Steps
    public InvalidResponse invalidresponse;

    @Then("he sees product name as {string} in RequestedItem")
    public void heSeesProductNameAsInRequestedItem(String product) {
        invalidresponse.assert_name_of_product(product);

    }

    @Then("he sees server name as {string} in ServedBy")
    public void heSeesServerNameAsInServedBy(String servedBy) {
        invalidresponse.assert_Served_By(servedBy);
    }

    @Then("he does not see the message")
    public void heDoesNotSeeTheMessage() {
        invalidresponse.assert_message_for_invalid_product(Constants.MESSAGE_INVALID_PRODUCT.getResources());
    }
}

package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;
import starter.bindings.ProductSearch;
import starter.utils.Constants;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {

    @Steps
    public ProductSearch productSearch;

    @When("he calls endpoint with {string}")
    public void heCallsEndpoint(String product) {
        productSearch.getResponse(product);
    }

    @Then("he does not see the results")
    public void he_Doesn_Not_See_The_Results() {
        productSearch.assert_no_result_received();

    }

    @Then("he verify total number of product retrieved is {int}")
    public void heVerifyResultSizeIsProductSize(int productCount) {
        productSearch.assert_number_of_product_received_in_response(productCount);

    }

    @Then("he sees Status Code as {int}")
    public void heSeesStatusCode(int statusCode) {
        productSearch.assert_status_code(statusCode);

    }

    @Then("he sees all URL has the respective {string} name")
    public void heSeesAllURLHasTheRespectiveName(String provider) {
        productSearch.urlValidation(provider);
    }




    @Then("he sees valid status code")
    public void heSeesValidStatusCode() {
        lastResponse().then().statusCode(Integer.parseInt(Constants.PASS_STATUS_CODE.getResources()));
    }

    @Then("he sees promo details for product isPromo is TRUE")
    public void heSeesPromoDetailsForProductIsPromoIsTRUE() {
        productSearch.assert_promo_details();
    }
}

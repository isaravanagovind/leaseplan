package starter.bindings;

import net.thucydides.core.annotations.Step;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.is;

public class InvalidResponse {

    @Step("Invalid product search Request Item - {0}")
    public void assert_name_of_product(String product) {
        restAssuredThat(response -> response.body("detail.requested_item", is(product)));
    }

    @Step("Invalid product search Served By - {0}")
    public void assert_Served_By(String servedBy) {
        restAssuredThat(response -> response.body("detail.served_by", is(servedBy)));
    }

    @Step("Invalid product search message should be {0}")
    public void assert_message_for_invalid_product(String message) {
        restAssuredThat(response -> response.body("detail.message", is(message)));
    }
}

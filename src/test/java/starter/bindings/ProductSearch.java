package starter.bindings;


import groovy.json.JsonOutput;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import starter.pojo.GetDetail;
import starter.pojo.GetProduct;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

public class ProductSearch {

    String baseURI = SystemEnvironmentVariables.createEnvironmentVariables().getProperty("webdriver.base.url");
    String pathURI = SystemEnvironmentVariables.createEnvironmentVariables().getProperty("webdriver.base.path");

    List<GetProduct> getProd = new ArrayList<>();
    GetDetail getdetails = new GetDetail();
    List<String> allTitle = new ArrayList<String>();
    List<String> allProviders = new ArrayList<String>();
    List<String> allURLs = new ArrayList<String>();
    List<Boolean> allPromo = new ArrayList<Boolean>();
    List<String> allPromoDetails = new ArrayList<String>();

    SoftAssertions sf = new SoftAssertions();

    @Step("GET method is called for the product - {0}")
    public void getResponse(String product) {

        try {
            getProd = Arrays.asList(SerenityRest.given()
                    .get(baseURI + pathURI + product)
                    .as(GetProduct[].class));

            for (GetProduct g : getProd) {
                allTitle.add(g.getTitle());
                allProviders.add(g.getProvider());
                allURLs.add(g.getUrl());
                allPromo.add(g.getIsPromo());
                allPromoDetails.add(g.getPromoDetails());
            }
        } catch (Exception e) {
            getdetails = SerenityRest.given()
                    .get(baseURI + pathURI + product)
                    .as(GetDetail.class);
        }
    }

    @Step("URL contains Provider name - {0}")
    public void urlValidation(String provider) {

        for (int i = 0; i < allProviders.size(); i++) {
            if (allProviders.get(i).equalsIgnoreCase(provider)) {
                allURLs.get(i).contains(provider);
            }
        }
    }

    @Step("No result - THEN error is TRUE")
    public void assert_no_result_received() {
        restAssuredThat(response -> response.body("detail.error", is(true)));
    }

    @Step("Number of product received in response should be {0}")
    public void assert_number_of_product_received_in_response(int productCount) {
        restAssuredThat(response -> response.body(String.valueOf("result".length()), hasSize(productCount)));
    }

    @Step("Status Code should be {0}")
    public void assert_status_code(int statusCode) {
        lastResponse().then().statusCode(statusCode);
    }

    @Step("Check Promo = True Provides Promo Details in response")
    public void assert_promo_details() {
        for (int i = 0; i < allProviders.size(); i++) {
            if (allPromo.get(i).equals(true)) {
                Assert.assertTrue("Response has Promo as TRUE But does not have Promo details", allPromoDetails.get(i).matches("^[a-zA-Z0-9]*$"));
            } else {
                Assert.assertTrue("Response has Promo as FALSE But have Promo details", allPromoDetails.get(i).matches(""));
            }

        }
    }
}

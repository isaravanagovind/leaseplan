package starter.utils;

public enum Constants {

    PASS_STATUS_CODE("200"),
    Err_STATUS_CODE("404"),
    MESSAGE_INVALID_PRODUCT("Not found");



    private String resources;

    Constants(String resources) {
        this.resources = resources;
    }

    public String getResources() {
        return resources;
    }


}

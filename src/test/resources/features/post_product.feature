Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  Scenario Outline: Get Response, Validate count of product list and URL Consists of Provider Name
    When he calls endpoint with "<Product>"
    Then he sees Status Code as <Status Code>
    Then he verify total number of product retrieved is <Product Size>
    Then he sees all URL has the respective "<Provider>" name

    Examples:
      | Product | Status Code | Product Size | Provider |
      | mango   | 200         | 116          | AH       |
      | apple   | 200         | 59           | Vomar    |
      | tofu    | 200         | 23           | Coop     |
      | water   | 200         | 151          | Jumbo    |



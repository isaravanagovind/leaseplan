Feature: Invalid response

  Scenario Outline: Validate Response for Invalid Product
    When he calls endpoint with "<Product>"
    Then he sees Status Code as <Status Code>
    Then he does not see the results
    Then he does not see the message
    Then he sees product name as "<Product>" in RequestedItem
    Then he sees server name as "<Served By>" in ServedBy

    Examples:
      | Product | Status Code | Served By            |
      | car     | 404         | https://waarkoop.com |
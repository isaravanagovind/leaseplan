Feature: Validate Promo Details

                    ####
                    ### if isPromo = TRUE Then promoDetails = Should Not be NULL #####
                    #### if isPromo = FALSE Then promoDetails = Should be NULL #####

  Scenario Outline: Promo Details
    When he calls endpoint with "<Product>"
    Then he sees valid status code
    Then he sees promo details for product isPromo is TRUE

    Examples:
      | Product |
      | tofu    |



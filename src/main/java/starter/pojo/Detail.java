package starter.pojo;

public class Detail {

    private Boolean error;
    private String message;
    private String requested_item;
    private String served_by;

    public Boolean getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public String getRequested_item() {
        return requested_item;
    }

    public String getServed_by() {
        return served_by;
    }
}



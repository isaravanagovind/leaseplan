package starter.pojo;

public class GetProduct {

    private String provider;
    private String title;
    private String url;
    private String brand;
    private Double price;
    private String unit;
    private Boolean isPromo;
    private String promoDetails;
    private String image;


    public String getProvider() {
        return provider;
    }


    public String getTitle() {
        return title;
    }


    public String getUrl() {
        return url;
    }


    public String getBrand() {
        return brand;
    }


    public Double getPrice() {
        return price;
    }


    public String getUnit() {
        return unit;
    }


    public Boolean getIsPromo() {
        return isPromo;
    }


    public String getPromoDetails() {
        return promoDetails;
    }


    public String getImage() {
        return image;
    }

}
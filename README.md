1. Deleted all GRADLE related stuffs and unwanted folders and files
    - build.gradle, LICENSE, history etc
    
2. POM file changes: Keeping only necessary  dependency and upgrading the versions
   
        - removed <tags> / distributionManagement /
        - moved webdriver.base.url to serenity.properties
        -  TestRunner.initializationError ? NoClassDefFound io/cucumber/core/runtime/Type. and other issues
                So removed serenity-cucumber6 and added serenity-cucumber latest

   
3. Feature File:
   
        Scenario Outline with Example is implemented
   
4. In SeacrchStepDefination class:
   
        - Removed methods like HARDCODED / NOT USED - Below are sample methods
                HardCoded mango:
                   @Then("he sees the results displayed for mango")
                   public void heSeesTheResultsDisplayedForMango() {
                   restAssuredThat(response -> response.body("title", contains("mango")));
                   }
                
                Status code is Verified Only for Apple Product: This Code is Refactored 
                      #####  Old Code #### 
                           @Then("he sees the results displayed for apple")
                           public void heSeesTheResultsDisplayedForApple() {
                           restAssuredThat(response -> response.statusCode(200));
                           }

                       #### Refactored code #####
                           @Then("he sees Status Code as {int}")
                           public void heSeesStatusCode(int statusCode) {
                           lastResponse().then().statusCode(statusCode);
                           }
   
5. Added .gitlab-ci.yml for CI
   
6. Implemented POJO [Plain Old Java Object] Class

    Reason for POJO Classes - To define response object entities.
   
    Here In this Project, POJO class is used for De-Serialization (Only Getter Methods)
   
        Example: For below response - Getter / Setter java class for each object entities [refer Detail.java & GetDetail.java]
   
            Note: here in this project we use only get command so only getters are used
   
               {
               "detail": {
               "error": true,
               "message": "Not found",
               "requested_item": "car",
               "served_by": "https://waarkoop.com"
               }
               }
   
            If object (method names) implemented in Detail.java & GetDetail.java has mismatch in object received in response, then the test fails
            Because of unknown objects is defined in classes

            if any unknown entities received in future - We can keep only mandatory fields in POJO classes and provide
            @JsonIgnoreProperties(ignoreUnknown = true)

            if validation for NOT NULL of all enitities in response then provide
            @JsonInclude(JsonInclude.Include.NON_NULL)

   
            Similarlly, if the recived response has new obejects - Then also the test fails
            Because object are not defined in classes

            And, All entities are declared with repective data types. 
            Example:
             "error": true, --> is declared as Boolean
             if error - received as String then Test will fail

             So The response dataType is validated in POJO classes - no need of seperate validation

7. Deleted serenity.config file - Not used in project

8. how to install and run
   
    Clone he project from GITLAB
    Goto Project folder
    Type "mvn clean verify"
   
    or
   
    This project is configured in GITLAB CI - Goto Pipeline and Click "RUN PIPELINE"
   
9. How to write new tests:
   
        Write Test Case: Create a feature file / Select appropriate feature file and start write scenario

        StepDefinition: For each Step in feature file create step defination in respective stepDefinition file

        Binding: Any logic can be written in Binding package [Create a file under this package]

        Constants: ENUM Class - Where all final constants can be defined

        If New Response / New Entities in response - Add / Create in pojo classes

   
10. promo.feature:
    
        Just a demo test case with below consideration
    
         Consider Below Validation:
                    #### if isPromo = TRUE Then promoDetails = Should not be NULL #####
                    #### if isPromo = FALSE Then promoDetails = Should be NULL #####
    
    
        Here Used Constant ENUM class for DEMO
        REGEX IS USED FOR ASSERTION to check any text presnt in promoDetails entity

11. Please note there is a flakiness in product response array size for the PRODUCT = MANGO
    
      Sometimes the count is 114 and sometimes 116
    
      So there may be a failure due to that.

12. About Test cases:
    
      post_invalid_product.feature - Validates invalid product search response
      post_product.feature.        - Validate product search and status code, received mandatory reponses using POJO clases
      promo.feature                - Sample test case with consideration if ISPROMO = TRUE then PROMODETAILS should not be NULL
    

12. Reports : target/site/serenity/index.html

**************** END OF FILE ******************

Look Forward to feedback :)
    
